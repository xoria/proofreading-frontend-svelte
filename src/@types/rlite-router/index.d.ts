declare module "rlite-router" {
  type Router = <Result>(
    error404: () => Result,
    pages: { [key: string]: (params: {[key: string]: string}) => Result },
  ) => (path: string) => Result;

  const router: Router;
  export default router;
}
